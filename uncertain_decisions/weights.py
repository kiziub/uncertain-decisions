# Copyright (c) 2021 Bartłomiej Kizielewicz

import numpy as np
from entropies.entropies_ifs import entropy_ifs_szmidt


def entropy_weights(matrix, method=entropy_ifs_szmidt):
    weights = np.zeros(matrix.shape[1])
    for i in range(matrix.shape[1]):
        weights[i] = method(matrix[:, i, :])

    return (1 - weights) / np.sum(1 - weights)
