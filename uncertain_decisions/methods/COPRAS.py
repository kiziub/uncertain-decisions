# Copyright (c) 2021 Bartłomiej Kizielewicz

import numpy as np


class COPRAS:

    def __init__(self):
        pass

    def __call__(self, matrix, weights, criteria_types, *args, **kwargs):
        return COPRAS._copras(matrix, weights, criteria_types)

    @staticmethod
    def _copras(matrix, weights, criteria_types):

        # Weighted matrix
        wmatrix = np.zeros(matrix.shape)
        wmatrix[:, :, 0] = np.sqrt(1 - (1 - matrix[:, :, 0] ** 2) ** weights)
        wmatrix[:, :, 1] = np.sqrt((matrix[:, :, 1] ** 2) ** weights)

        # Score function
        s = wmatrix[:, :, 0] ** 2 - wmatrix[:, :, 1] ** 2

        # Determine the maximizing and minimizing index
        Sp = np.sum(s[:, criteria_types == 1], axis=1) / criteria_types[criteria_types == 1].shape
        Sr = np.sum(s[:, criteria_types == -1], axis=1) / criteria_types[criteria_types == -1].shape

        # Determine the relative significance value of each alternative
        N = np.sum(np.exp(Sr)) / np.sum(1 / np.exp(Sr))
        Q = Sp + (N / np.exp(Sr))

        return Q / np.max(Q) * 100
