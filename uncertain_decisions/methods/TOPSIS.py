# Copyright (c) 2021 Bartłomiej Kizielewicz

import numpy as np


class INV_TOPSIS:

    def __init__(self):
        pass

    def __call__(self, matrix, weights, criteria_types, *args, **kwargs):
        return INV_TOPSIS._inv_topsis(matrix, weights, criteria_types)

    @staticmethod
    def _inv_topsis(matrix, weights, criteria_types):
        # Normalized matrix
        nmartix = np.zeros(matrix.shape)
        nmartix[:, :, 0] = matrix[:, :, 0] / np.sqrt(np.sum(matrix[:, :, 0] ** 2 + matrix[:, :, 1] ** 2, axis=0))
        nmartix[:, :, 1] = matrix[:, :, 1] / np.sqrt(np.sum(matrix[:, :, 0] ** 2 + matrix[:, :, 1] ** 2, axis=0))

        # Weighted matrix
        wmatrix = np.zeros(matrix.shape)
        wmatrix[:, :, 0] = nmartix[:, :, 0] * weights
        wmatrix[:, :, 1] = nmartix[:, :, 1] * weights

        # Distance form positive and negative ideal solution
        dp = np.sqrt(
            np.sum((wmatrix[:, criteria_types == -1, 1] - np.min(wmatrix[:, criteria_types == -1, 0], axis=0)) ** 2,
                   axis=1) +
            np.sum((wmatrix[:, criteria_types == 1, 0] - np.max(wmatrix[:, criteria_types == 1, 1], axis=0)) ** 2,
                   axis=1))
        dn = np.sqrt(
            np.sum((wmatrix[:, criteria_types == -1, 0] - np.max(wmatrix[:, criteria_types == -1, 1], axis=0)) ** 2,
                   axis=1) +
            np.sum((wmatrix[:, criteria_types == 1, 1] - np.min(wmatrix[:, criteria_types == 1, 0], axis=0)) ** 2,
                   axis=1))

        return dn / (dp + dn)


class PFS_TOPSIS:

    def __init__(self):
        pass

    def __call__(self, matrix, weights, *args, **kwargs):
        return PFS_TOPSIS._pfs_topsis(matrix, weights)

    @staticmethod
    def _get_distance(matrix, ideal_solution, weights):
        dmi = np.abs(matrix[:, :, 0] ** 2 - ideal_solution[:, 0] ** 2)
        dvi = np.abs(matrix[:, :, 1] ** 2 - ideal_solution[:, 1] ** 2)
        pi = np.sqrt(1 - matrix[:, :, 0] ** 2 - matrix[:, :, 1] ** 2)
        pi_ideal = np.sqrt(1 - ideal_solution[:, 0] ** 2 - ideal_solution[:, 1] ** 2)
        dpi = np.abs(pi ** 2 - pi_ideal ** 2)
        return np.sum(weights * (dmi + dvi + dpi), axis=1) / 2

    @staticmethod
    def _pfs_topsis(matrix, weights):
        s = matrix[:, :, 0] ** 2 - matrix[:, :, 1] ** 2

        arg_pis = np.argmax(s, axis=0)
        arg_nis = np.argmin(s, axis=0)

        pis = matrix[arg_pis, np.arange(s.shape[1]), :]
        nis = matrix[arg_nis, np.arange(s.shape[1]), :]

        dpis = PFS_TOPSIS._get_distance(matrix, pis, weights)
        dnis = PFS_TOPSIS._get_distance(matrix, nis, weights)

        return dnis / np.max(dnis) - dpis / np.min(dpis)
