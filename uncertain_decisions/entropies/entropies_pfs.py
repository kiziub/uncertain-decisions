# Copyright (c) 2021 Bartłomiej Kizielewicz

import numpy as np


def entropy_pfs_zhang(x):
    return -(np.sum(
        (x[:, 0] + 1 - x[:, 1]) / 2 * np.log10((x[:, 0] + 1 - x[:, 1]) / 2) + ((x[:, 1] + 1 - x[:, 0]) / 2) * np.log10(
            (x[:, 1] + 1 - x[:, 0]) / 2))) / len(x[:, 0])


def entropy_pfs_xue(x):
    return np.sum(1 - (x[:, 0] ** 2 + x[:, 1] ** 2) * np.abs(x[:, 0] ** 2 - x[:, 1] ** 2)) / x.shape[0]


def entropy_pfs_wei(x):
    return np.sum((np.sqrt(2) * np.cos((x[:, 0] - x[:, 1]) / 4 * np.pi) - 1) * (1 / (np.sqrt(2) - 1))) / x.shape[0]


def entropy_pfs_liu(x):
    xi = np.pi / 4 + (np.abs(x[:, 0] - x[:, 1]) * (1 - (1 - x[:, 0] - x[:, 1]))) / 4 * np.pi
    return np.sum(np.cos(xi) / np.sin(xi)) / x.shape[0]


def entropy_pfs_wang(x):
    xi = np.pi / 4 + np.abs(x[:, 0] - x[:, 1]) / (4 * (1 + (1 - x[:, 0] - x[:, 1]))) * np.pi
    return np.sum(np.cos(xi) / np.sin(xi)) / x.shape[0]


def entropy_pfs_gandotra(x):
    return np.sum((1 / np.cos(np.pi / 3 - np.abs(x[:, 0] ** 2 - x[:, 1] ** 2) * np.pi / 3)) - 1) / x.shape[0]
