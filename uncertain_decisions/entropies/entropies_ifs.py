# Copyright (c) 2021 Bartłomiej Kizielewicz

import numpy as np


def entropy_ifs_thakur(x):
    return np.sum((1 / np.cos(np.abs(np.abs(3 - 2 * x[:, 0] - 7 / 3) - 7 / 3) * np.pi / 7) + 1 / np.cos(
        np.abs(np.abs(3 - 2 * x[:, 1] - 7 / 3) - 7 / 3) * np.pi / 7) - 334 / 135) / (206 / 135)) / x.shape[0]


def entropy_ifs_ye(x):
    return np.sum((np.sin((1 + x[:, 0] - x[:, 1]) / 4 * np.pi) + np.sin((1 + x[:, 1] - x[:, 0]) / 4 * np.pi) - 1) * (
            1 / (np.sqrt(2) - 1))) / x.shape[0]


def entropy_ifs_burillo(x):
    return np.sum(1 - (x[:, 0] + x[:, 1])) / x.shape[0]


def entropy_ifs_szmidt(x):
    pi = 1 - x[:, 0] - x[:, 1]
    return np.sum((np.min(x) + pi) / (np.max(x) + pi)) / x.shape[0]


def entropy_ifs_liu(x):
    value = (np.pi / 4) + np.abs(x[:, 0] ** 2 - x[:, 1] ** 2) / 4 * np.pi
    return np.sum(np.cos(value) / np.sin(value)) / x.shape[0]
